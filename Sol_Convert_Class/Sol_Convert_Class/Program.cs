﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Convert_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            int intVal = 401;
            string stringValResult = Converter.ToConvert(typeof(string), intVal);
            Console.WriteLine(stringValResult);

            int intValResult= Converter.ToConvert(typeof(int), stringValResult);
            Console.WriteLine(intValResult);

            string val1 = "345";
            int val1Result = Converter.ToConvert(typeof(int), val1);
            Console.WriteLine(val1);
        }
    }

    public static class Converter
    {
        public static dynamic ToConvert(Type typeOfObj,object value)
        {
            return Convert.ChangeType(value, typeOfObj);
        }
    }
}
